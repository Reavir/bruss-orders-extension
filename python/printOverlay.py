from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
import csv
from config import *
from printer import os_print_pdf


def generate_custom_pdf(values, path):
    # Create a PDF document
    c = canvas.Canvas(path)

    font_name = "Courier"
    font_size = 15
    c.setFont(font_name, font_size)

    width, height = A4

    text = 'ORDER: ' + values[0]
    x = font_size
    y = font_size
    c.drawString(x, y, text)

    text = values[2]
    text_width = c.stringWidth(text, font_name, font_size)
    text_width = c.stringWidth(text, font_name, font_size)
    x = (width - text_width) / 2
    y = font_size
    c.drawString(x, y, text)

    text = 'from: ' + values[4]
    text_width = c.stringWidth(text, font_name, font_size)
    x =  width - text_width - font_size
    y = font_size
    c.drawString(x, y, text)

    text = values[3]
    text_width = c.stringWidth(text, font_name, font_size)
    text_width = c.stringWidth(text, font_name, font_size)
    x = (width - text_width) / 2
    y = height - font_size - 30
    c.drawString(x, y, text)

    c.save()


if __name__ == "__main__":  
    rows = []

    with open(CSV_PATH, 'r') as file:
        reader = csv.reader(file, delimiter=';')
        for row in reader:
            rows.append(row)

    print('Zapisywanie...')
    for row in rows:
        if row[0] != '':
            print(row)
            path = OVERLAY_PATH + '\\' + row[0] + ' ' + row[1] + '.pdf'
            generate_custom_pdf(row, path)
    
    printing = input("Drukować? (T/N)\n")
    if printing == 't' or printing == 'T':
        for row in rows:
            if row[0] != '':
                print(row)
                path = OVERLAY_PATH + '\\' + row[0] + ' ' + row[1] + '.pdf'
                os_print_pdf(path)
    