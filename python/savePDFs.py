from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime
from bs4 import BeautifulSoup
import base64
import json
import re
import os
import keyboard
import csv
import getpass
import bcrypt

from config import *


RINGS_LIST = [
    'HDM 264',
    'DRM 32x8,6',
    'DRM 26x7,5',
    'KS 27,27x38x5,8',
    'KS 42 2',
    'KF 45,8 2',
    'KF 50,8 1',
    'KF 51,8',
    'KF 40x55',
    'KF 23,7x53',
    'KS 37,75x58x8',
    'KS 60 4',
    'KS 66 10',
    'KS 55 10',
    'KS 58,8',
    'KS 51x60x7',
    'KS 52 4',
    'KF 38x56',
    'KF 60,7 1',
    'KS 68 10',
    'KF 59,8x69x6,1',
    'KF 70,8',
    'KS 44x67',
    'KF 70,8',
    'KS 44x67',
    'KF 39,4x70',
    'KF 44,9x70',
    'KF 38x72',
    'KF 38x62',
    'KS 35x62',
    'KS 59,5x72x8,2',
    'KS 76',
    'KF 50x76',
    'KF 78,1',
    'KS 100,1',
    'KF 103,7 4',
    'KS 125 1',
    'KF 98x113x10,8',
    'KS 76',
    'KF 100x120'
]

def parse_article_name(str):
    article = str[6:-3]
    if article[0:2] == 'VB':
        article = article[3:]

    for substing in ['<', '>', ':', '"', '/', '\\', '|', '?', '*', '-']:
        if substing in article:
            article = article.replace(substing, ' ')

    if article[0:3] == 'HDM':
        article = article.replace('HDM', 'HDM ')
    elif article[0] == 'N':
        article = article.replace('N', 'N ')
    elif article[0] == 'J':
        article = article.replace('J', 'J ')
    elif article[3] != ' ':
        article = article.replace(article[0:2], article[0:2] + ' ') 

    while article[-1] == ' ':
        article = article[:-1]

    while '  ' in article:
        article = article.replace('  ', ' ') 

    return article


def login_bruss(username, password):
    # Set up Chrome options
    chrome_options = Options()
    chrome_options.add_argument("--log-level=3")
    chrome_options.add_argument("--headless")  # Run Chrome in headless mode (without GUI)
    chrome_options.add_argument("--remote-debugging-port=9222")

    # Set up Chrome driver
    driver = webdriver.Chrome(options=chrome_options)

    # Navigate to the login page
    driver.get("https://support.bruss-group.com/gbruss/index.php/en/")  # Replace with the URL of the login page

    # Fill in the login form
    username_input = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "modlgn-username")))
    password_input = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "modlgn-passwd")))
    login_button = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "input[name='Submit']")))

    username_input.send_keys(username)  # Replace with your actual username
    password_input.send_keys(password)  # Replace with your actual password

    # Submit the form
    login_button.click()

    return driver


def get_data():
    # Get a list of all files in the folder
    files = os.listdir(INPUT_PATH)

    json_files = [file for file in files if file.startswith('storedInputValues') and file.endswith('.json')]

    json_files.sort(key=lambda x: os.path.getmtime(INPUT_PATH + '\\' + x), reverse=True)

    newest_file = json_files[0]

    for file in json_files[1:]:
        file_path = INPUT_PATH + '\\' + file
        os.remove(file_path)   

    with open(INPUT_PATH + '\\' + newest_file, 'r') as file:
        input_values = json.load(file)

    values = input_values['values'].split(',')

    return values


def append_kanban_number(values):
    files = os.listdir(INPUT_PATH)

    filtered_files = [file for file in files if file.startswith('filtered_values') and file.endswith('.csv')]

    filtered_files.sort(key=lambda x: os.path.getmtime(INPUT_PATH + '\\' + x), reverse=True)

    latest_file = filtered_files[0]

    for file in filtered_files[1:]:
        file_path = INPUT_PATH + '\\' + file
        os.remove(file_path)

    rows = []
    with open(INPUT_PATH + '\\' + latest_file, 'r') as file:
        reader = csv.reader(file, delimiter=';')
        for row in reader:
            rows.append(row)
    
    for i in range(len(rows)):
        rows[i][6] = values[i]

    with open(FOLDER_PATH + '\\'  'kolejność_excel.csv', 'w+', newline='') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerows(rows)

    print(FOLDER_PATH + '\\'  'kolejność_excel.csv', "uaktualniony o numery KANBAN")


def get_names_from_html(html_content):
    # Parse the HTML using BeautifulSoup
    soup = BeautifulSoup(html_content, 'html.parser')

    # Extract the relevant information from the parsed HTML
    article = soup.find('div', class_='articleWrapper')
    article = article.contents[-1].strip()
    article = parse_article_name(article)
    kanban = soup.find('div', class_='articleWrapper').find_next('div', class_='articleWrapper').text.strip()
    kanban_2 = kanban.split(' ')[-1]
    kanban = kanban.split(' ')[1]
    date = soup.find('div', class_='infoKbDiv deliveryWrapper').get_text(strip=True)
    date = re.search(r'\((\d+\s\w+\s\d+)\)', date).group(1)
    date = datetime.strptime(date, "%d %b %Y")
    date = date.strftime("%d.%m.%Y")
    order_id = soup.find('div', class_='orderIdWrapper')
    order_id = order_id.find_all('span')[1].text.strip()
    if not any(e in article for e in RINGS_LIST):
        with open(CSV_PATH, 'a', newline='') as file:
            writer = csv.writer(file, delimiter=';')
            writer.writerow([order_id, article, f'K{kanban}' + '/' + f'{kanban_2}', date, datetime.now().strftime('%d.%m.%Y')])

    return f"{article} K{kanban} {date}.pdf", kanban_2


def save_PDFs(driver, values):
    print("Zapisywanie do:", PDF_SAVE_PATH)
    kanban_values = []
    for value in values:
        if value == '':
            continue

        # Generate the URL using the values
        url = f"https://support.bruss-group.com/gbruss/index.php/en/?option=com_supplierorders&view=confirmation&print=1&tmpl=component&order_id={value}&plant=700&"

        # Navigate to the URL
        driver.get(url)

        # Get the HTML content of the page
        html_content = driver.page_source

        # Generate the file name using the extracted information
        file_name, kanban = get_names_from_html(html_content)
        kanban_values.append(kanban)
        # Construct the full file path
        print(file_name)
        full_file_path = PDF_SAVE_PATH + '\\' + file_name

        # Save the PDF of the page
        pdf_content = driver.execute_cdp_cmd("Page.printToPDF", {})
        with open(full_file_path, "wb+") as pdf_file:
            decoded_content = base64.b64decode(pdf_content['data'])
            pdf_file.write(decoded_content)

    append_kanban_number(kanban_values)


def verify_password(password, hashed_password):
    return bcrypt.checkpw(password.encode('utf-8'), hashed_password)


if __name__ == "__main__":
    data = get_data()
    credentials = False
    
    username = b'$2b$12$cojZyLBvqy8A/Ok2Z.5dUOE244Ce9BD368pkRh7C03L/wRUFfQvKi'
    password = b'$2b$12$xSW/YUlm5qea0dkQcWmrVulcmeTSqsVg6vks4g.swETtXyRhmZjvG'
    input_username :str = ''
    input_password :str = ''

    with open(CSV_PATH, 'w+') as file:
        file.write('')

    while not credentials:
        input_username = getpass.getpass("Login: ")
        input_password = getpass.getpass("Hasło: ")

        if verify_password(input_username, username) and verify_password(input_password, password):
            credentials = True
        else:
            print('Niepoprawny login lub hasło.')
    
    driver = login_bruss(input_username, input_password)
    save_PDFs(driver, data)
    driver.quit()

    print("Naciśnij spację, aby zakończyć działanie programu.")
    keyboard.wait('space')
