document.addEventListener('DOMContentLoaded', function() {
  let prefix = localStorage.getItem('prefix');
  if (!prefix) {
    prefix = "000";
    localStorage.setItem('prefix', prefix);
  }
  document.getElementById('prefix').value = prefix;

  var saveFilteredButton = document.getElementById('saveFilteredButton');
  saveFilteredButton.addEventListener('click', function() {
    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
      var tabId = tabs[0].id;
      chrome.tabs.executeScript(tabId, { file: 'content.js' }, function() {
        chrome.tabs.sendMessage(tabId, { action: 'viewHTML' }, function(response) {
          if (response && response.html) {
            var storedValues = getStoredInputValues();
            var modifiedHTML = removeColumns(response.html, storedValues);
            var tableContainer = document.getElementById('tableContainer');
            tableContainer.style.display = 'block';
            tableContainer.innerHTML = modifiedHTML;
            var storedInputValues = getStoredInputValues();
            if (storedInputValues) {
              var inputValues = storedInputValues.values;
              var filteredValues = filterTable(inputValues);
              saveFilteredValues(filteredValues);
            }
          }
        });
      });
    });
  });

  var filterButton = document.getElementById('filterButton');
  filterButton.addEventListener('click', function() {
    var inputValues = document.getElementById('inputValues').value;
    prefix = document.getElementById('prefix').value;
    localStorage.setItem('prefix', prefix);

    // Store the input values in localSotrage
    storeInputValues(inputValues, selectedPrefix);
  });
  
  var addTofilterButton = document.getElementById('addTofilterButton');
  addTofilterButton.addEventListener('click', function() {
    var inputValues = document.getElementById('inputValues').value;
    var prefix = document.getElementById('prefix').value;
    localStorage.setItem('prefix', prefix);

    addInputValues(inputValues, prefix);
  });
  var resultContainer = document.getElementById('resultContainer'); 
  var viewStorageButton = document.getElementById('viewStorageButton');
  viewStorageButton.addEventListener('click', function() {
    var storedValues = getStoredInputValues();
    if (storedValues) {
      var inputValues = storedValues.values;
      var message = inputValues;
      var divToShow = document.getElementById('resultContainer');
      resultContainer.style.display = 'block';
      resultContainer.textContent = message;
      
    } else {
      resultContainer.textContent = 'No stored input values found!';
    }
  });

  var openURLsButton = document.getElementById('openURLs');
  openURLsButton.addEventListener('click', function() {
    var storedValues = getStoredInputValues();
    if (storedValues) {
      var inputValues = storedValues.values.split(',');
      var index = 0;
      var tabIds = [];

      function openNextURL() {
        if (index < inputValues.length) {
          var input = inputValues[index].trim();
          var url = 'https://support.bruss-group.com/gbruss/index.php/en/?option=com_supplierorders&view=confirmation&order_id=' + input + '&plant=700';

          chrome.tabs.create({ url: url, active: false }, function(tab) {
            tabIds.push(tab.id);
            index++;
            openNextURL(); // Proceed to the next URL immediately
          });
        } else {
          // All tabs have been created, now wait for them to finish loading
          checkTabsStatus();
        }
      }

      function checkTabsStatus() {
        chrome.tabs.onUpdated.addListener(function listener(updatedTabId, changeInfo) {
          if (changeInfo.status === 'complete' && tabIds.includes(updatedTabId)) {
            chrome.tabs.onUpdated.removeListener(listener);
            var tabIndex = tabIds.indexOf(updatedTabId);
            chrome.tabs.remove(updatedTabId); // Close the tab
            tabIds.splice(tabIndex, 1); // Remove the tab ID from the array

            if (tabIds.length === 0) {
              chrome.tabs.reload();
            } else {
              checkTabsStatus(); // Continue checking the status of the remaining tabs
            }
          }
        });
      }

      openNextURL();
    } else {
      alert('No stored input values found!');
    }
  });
});

function removeColumns(html, storedValues) {
  var tempContainer = document.createElement('div');
  tempContainer.innerHTML = html;

  var table = tempContainer.getElementsByTagName('table')[0];
  var rows = table.rows;
  table.deleteRow(0);
  if (storedValues) {
    var inputValues = storedValues.values.split(',');

    for (var i = rows.length - 1; i >= 0; i--) {
      var row = rows[i];
      var idCell = row.cells[1];
      var id = idCell.textContent.trim();
      var hasMatchingValue = inputValues.some(function(value) {
        return id.includes(value.trim());
      });
      if (!hasMatchingValue) {
        table.deleteRow(i);
      }
    }
  }
  
  for (var i = 0; i < rows.length; i++) {
    rows[i].deleteCell(0);
    rows[i].deleteCell(1);
    rows[i].deleteCell(5);
    rows[i].deleteCell(5);
    rows[i].deleteCell(6);
    rows[i].deleteCell(2);
  }
  //Move column 5 to index 0
  for (var i = 0; i < rows.length; i++) {
    var cells = rows[i].cells;
    var cellToMove = cells[4].cloneNode(true);
    rows[i].deleteCell(4);
    rows[i].insertBefore(cellToMove, rows[i].cells[0]);
  }
  //Move column 4 to index 5
  for (var i = 0; i < rows.length; i++) {
    var cells = rows[i].cells;
    var cellToMove = cells[3].cloneNode(true);
    rows[i].deleteCell(3);
    rows[i].insertBefore(cellToMove, rows[i].cells[5]);
  }
  // Insert empty column at index 3
  for (var i = 0; i < rows.length; i++) {
    var newRow = rows[i];
    var emptyCell = newRow.insertCell(3);
    emptyCell.textContent = ""; // Set the empty cell value
  }
  // Insert empty column at index 6
  for (var i = 0; i < rows.length; i++) {
    var newRow = rows[i];
    var emptyCell = newRow.insertCell(6);
    emptyCell.textContent = ""; // Set the empty cell value
  }
  // Insert column with todays date at index 7
  for (var i = 0; i < rows.length; i++) {
    var newRow = rows[i];
    var dateCell = newRow.insertCell(7);
    dateCell.textContent = getFormattedDate(); // Set the value to today's date
  }
  // Format the date in the first column
  for (var i = 0; i < rows.length; i++) {
    var cells = rows[i].cells;
    var firstColumnDate = cells[0].textContent;
    var formattedDate = formatDate(firstColumnDate);
    cells[0].textContent = formattedDate;
  }

  return tempContainer.innerHTML;
}

function getFormattedDate() {
  var today = new Date();    
  var year = today.getFullYear();
  var month = String(today.getMonth() + 1).padStart(2, '0');
  var day = String(today.getDate()).padStart(2, '0');
  return year + '-' + month + '-' + day;
}

function getCurrentTime() {
  var currentTime = Math.floor(Date.now() / 1000);
  return currentTime;
}

function formatDate(dateString) {
  var date = new Date(dateString);
  var year = date.getFullYear();
  var month = String(date.getMonth() + 1).padStart(2, '0');
  var day = String(date.getDate()).padStart(2, '0');
  return year + '-' + month + '-' + day;
}

function filterTable(inputValues) {
  var tempContainer = document.createElement('div');
  tempContainer.innerHTML = document.getElementById('tableContainer').innerHTML;

  var table = tempContainer.querySelector('table');
  var rows = table.querySelectorAll('tbody > tr');
  
  var idsToInclude = inputValues.split(',').map(function(value) {
    return  value.trim();
  });

  var filteredTable = document.createElement('table');
  var tbody = document.createElement('tbody');
  filteredTable.appendChild(tbody);

  // Filter the rows based on input values
  for (var i = 0; i < rows.length; i++) {
    var row = rows[i];
    var idCell = row.cells[1];
    var id = idCell.textContent.trim();
    if (idsToInclude.indexOf(id) !== -1) {
      var filteredRow = row.cloneNode(true);
      tbody.appendChild(filteredRow);
    }
  }

  return filteredTable.outerHTML;
}

function saveFilteredValues(filteredValues) {
  var csvContent = convertTableToCSV(filteredValues);

  // Create a blob with the CSV content
  var blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });

  // Create a temporary download link
  var downloadLink = document.createElement('a');
  downloadLink.href = URL.createObjectURL(blob);
  downloadLink.download = 'filtered_values.csv';

  // Trigger the download
  downloadLink.click();

  // Clean up the temporary download link
  URL.revokeObjectURL(downloadLink.href);
}

function convertTableToCSV(tableHTML) {
  var tempContainer = document.createElement('div');
  tempContainer.innerHTML = tableHTML;

  var rows = tempContainer.querySelectorAll('tr');
  var csvContent = '';

  for (var i = 0; i < rows.length; i++) {
    var cells = rows[i].querySelectorAll('td');
    var rowValues = [];

    for (var j = 0; j < cells.length; j++) {
      var cellValue = cells[j].textContent.trim();
      rowValues.push(cellValue);
    }

    csvContent += rowValues.join(';') + '\n';
  }

  return csvContent;
}

function storeInputValues(inputValues, prefix) {
  var valuesArray = inputValues.split(','); // Split the input values by comma
  var processedValues = [];

  // Process each input value
  valuesArray.forEach(function (value) {
    value = value.trim(); // Remove any leading or trailing spaces

    if (value.indexOf('-') !== -1) {
      // If the value is a range, extract the start and end values
      var range = value.split('-');
      var start = parseInt(range[0]);
      var end = parseInt(range[1]);

      // Generate individual values within the range and add them to the processed values
      for (var i = start; i <= end; i++) {
        processedValues.push(String(i).padStart(range[0].length, '0'));
      }
    } else {
      // If it's a single value, add it to the processed values
      processedValues.push(value);
    }
  });

  var storedInputValues = {
    values: processedValues.map(value => prefix + value).join(',')
  };
  localStorage.setItem('inputValues', JSON.stringify(storedInputValues));
}

function addInputValues(inputValues, prefix) {
  var valuesArray = inputValues.split(','); // Split the input values by comma
  var processedValues = [];

  // Process each input value
  valuesArray.forEach(function (value) {
    value = value.trim(); // Remove any leading or trailing spaces

    if (value.indexOf('-') !== -1) {
      // If the value is a range, extract the start and end values
      var range = value.split('-');
      var start = parseInt(range[0]);
      var end = parseInt(range[1]);

      // Generate individual values within the range and add them to the processed values
      for (var i = start; i <= end; i++) {
        processedValues.push(String(i).padStart(range[0].length, '0'));
      }
    } else {
      // If it's a single value, add it to the processed values
      processedValues.push(value);
    }
  });

  var storedInputValues = getStoredInputValues();

  var NewStoredInputValues = {
    values: processedValues.map(value => prefix + value).join(',')
  };
  storedInputValues.values = storedInputValues.values + ',' + NewStoredInputValues.values;

  localStorage.setItem('inputValues', JSON.stringify(storedInputValues));
}

function getStoredInputValues() {
  var storedValues = localStorage.getItem('inputValues');
  if (storedValues) {
    return JSON.parse(storedValues);
  }
  return null;
}

function dataUrlToBlobUrl(dataUrl)  {
  var byteString = atob(dataUrl.split(',')[1]);
  var mimeString = dataUrl.split(',')[0].split(':')[1].split(';')[0];
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }
  var blob = new Blob([ab], { type: mimeString });
  return URL.createObjectURL(blob);
}