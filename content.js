chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.action === 'viewHTML') {
    var tables = document.getElementsByTagName('table');
    if (tables.length >= 2) {
      var tableHTML = tables[1].outerHTML;
      sendResponse({ html: tableHTML });
    }
  }
});
